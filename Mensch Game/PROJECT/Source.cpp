#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include "Game.h"

void RunMenu(int PlayerConndition[], sf::Color PlayerColor[]); 

int main()
{
	int PlayerConndition[4];
	sf::Color PlayerColor[4];
	RunMenu(PlayerConndition, PlayerColor);

	Game Ludo{ PlayerConndition, PlayerColor };
	Ludo.Run();
	return 0;
}

void RunMenu(int PlayerConndition[], sf::Color PlayerColor[])
{
	std::cout << "sHLudoHs" << std::endl
		<< "How do you want to run the game? (1: with menu; 2: default style) ";
	unsigned int MenuChecker{};
	std::cin >> MenuChecker;
	switch (MenuChecker)
	{
	case 1:
		/* Original Style */
		std::cout << "PlayerConnditions (-1: PC; 0: None; +1: Player)" << std::endl;
		for (size_t i = 0; i < 4; i++)
		{
			std::cout << "Player " << i << ": ";
			std::cin >> PlayerConndition[i];
		}
		std::cout << std::endl << "PlayerColor (Red, Green, Blue, Yellow, Magenta, Cyan)" << std::endl;
		for (size_t i = 0; i < 4; i++)
			if (PlayerConndition[i] != 0)
			{
				std::cout << "Player " << i << ": ";
				std::string str;
				std::cin >> str;
				if (str == "Red")
					PlayerColor[i] = sf::Color::Red;
				else if (str == "Green")
					PlayerColor[i] = sf::Color::Green;
				else if (str == "Blue")
					PlayerColor[i] = sf::Color::Blue;
				else if (str == "Yellow")
					PlayerColor[i] = sf::Color::Yellow;
				else if (str == "Magenta")
					PlayerColor[i] = sf::Color::Magenta;
				else if (str == "Cyan")
					PlayerColor[i] = sf::Color::Cyan;
			}
		break;
	case 2:
		/* TestStyle */
		PlayerConndition[0] = +1;
		PlayerConndition[1] = +1;
		PlayerConndition[2] = +1;
		PlayerConndition[3] = +1;
		PlayerColor[0] = sf::Color::Red;
		PlayerColor[1] = sf::Color::Blue;
		PlayerColor[2] = sf::Color::Green;
		PlayerColor[3] = sf::Color::Magenta;
		break;
	default:
		break;
	}
	std::cout << std::endl;
	
	return;
}