#pragma once
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <fstream>
#include <SFML/Graphics.hpp>
#include "player.h"
#include "point.h"

class Game
{
public:
	Game(int PlayerCondition[], sf::Color PlayerColor[]);
	void Run();
	~Game();

	/* Create */
	void CreatePlayers();
	void CreatePointsCoordinate();
	void CreateDiceButton();
	void CreateOriginalPoints();

	/* GameLoop */
	void ProcessEvents();
	bool theGameisRunning();
	void DrawWindow();
	void MouseClickDiceFunction();
	void TokenHasBeenChoosenFunction();
	void SetHandout();
	bool isMovable(unsigned int& TokenNum);
	bool isMute();
	void MoveToken();
	void WinCheck();
	void ChooseToken();

	/* OriginalVariables */
	Player player[4];
	int PlayerCondition[4]; // -1: PC; 0: None; +1: Player
	sf::Color PlayerColor[4];
	sf::Vector2f PointCoordinate[11][11];
	Point OriginalPoint[40];
	sf::CircleShape DiceButton;

	/* Window */
	sf::ContextSettings settings{ 0, 0, 8 };
	sf::RenderWindow window{ sf::VideoMode(800, 600), "Ludo", sf::Style::Close, settings };

	/* GameLoopVariables */
	int handout{ -1 }; // before start
	unsigned int TokenNumber{};
	unsigned int DiceNumber{};
	bool WanttoChooseToken{ false };
	bool WanttoClickDice{ true };
	bool TokenHasBeenChoosen{ false };
	bool MouseClickDice{ false };
};
