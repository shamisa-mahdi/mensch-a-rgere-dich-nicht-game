#include "player.h"
#include <SFML/Graphics.hpp>

Player::Player()
{
}

Player::Player(size_t i , const sf::Color& PColor)
{
	index = i;
	color = PColor;
}


Player::~Player()
{
}

void Player::CreatePoints(sf::Vector2f PointCoordinate[11][11])
{
	float R{ 20. };

	/* Common */
	for (size_t iter{}; iter < 4; iter++)
	{
		OutsidePoint[iter].setFillColor(sf::Color::White);
		OutsidePoint[iter].setRadius(R);
		OutsidePoint[iter].setOrigin(R, R);
		OutsidePoint[iter].setOutlineColor(color);
		OutsidePoint[iter].setOutlineThickness(4);

		InsidePoint[iter].setFillColor(sf::Color::White);
		InsidePoint[iter].setRadius(R);
		InsidePoint[iter].setOrigin(R, R);
		InsidePoint[iter].setOutlineColor(color);
		InsidePoint[iter].setOutlineThickness(4);
	}

	/* setPosition */
	switch (index)
	{
	case 0:
		OutsidePoint[0].setPosition(PointCoordinate[10][10].x + R, PointCoordinate[10][10].y + R);
		OutsidePoint[1].setPosition(PointCoordinate[10][9].x + R, PointCoordinate[10][9].y + R);
		OutsidePoint[2].setPosition(PointCoordinate[9][9].x + R, PointCoordinate[9][9].y + R);
		OutsidePoint[3].setPosition(PointCoordinate[9][10].x + R, PointCoordinate[9][10].y + R);
		InsidePoint[0].setPosition(PointCoordinate[5][9].x + R, PointCoordinate[5][9].y + R);
		InsidePoint[1].setPosition(PointCoordinate[5][8].x + R, PointCoordinate[5][8].y + R);
		InsidePoint[2].setPosition(PointCoordinate[5][7].x + R, PointCoordinate[5][7].y + R);
		InsidePoint[3].setPosition(PointCoordinate[5][6].x + R, PointCoordinate[5][6].y + R);
		break;

	case 1:
		OutsidePoint[0].setPosition(PointCoordinate[10][1].x + R, PointCoordinate[10][1].y + R);
		OutsidePoint[1].setPosition(PointCoordinate[10][0].x + R, PointCoordinate[10][0].y + R);
		OutsidePoint[2].setPosition(PointCoordinate[9][0].x + R, PointCoordinate[9][0].y + R);
		OutsidePoint[3].setPosition(PointCoordinate[9][1].x + R, PointCoordinate[9][1].y + R);
		InsidePoint[0].setPosition(PointCoordinate[9][5].x + R, PointCoordinate[9][5].y + R);
		InsidePoint[1].setPosition(PointCoordinate[8][5].x + R, PointCoordinate[8][5].y + R);
		InsidePoint[2].setPosition(PointCoordinate[7][5].x + R, PointCoordinate[7][5].y + R);
		InsidePoint[3].setPosition(PointCoordinate[6][5].x + R, PointCoordinate[6][5].y + R);
		break;

	case 2:
		OutsidePoint[0].setPosition(PointCoordinate[1][1].x + R, PointCoordinate[1][1].y + R);
		OutsidePoint[1].setPosition(PointCoordinate[1][0].x + R, PointCoordinate[1][0].y + R);
		OutsidePoint[2].setPosition(PointCoordinate[0][0].x + R, PointCoordinate[0][0].y + R);
		OutsidePoint[3].setPosition(PointCoordinate[0][1].x + R, PointCoordinate[0][1].y + R);
		InsidePoint[0].setPosition(PointCoordinate[5][1].x + R, PointCoordinate[5][1].y + R);
		InsidePoint[1].setPosition(PointCoordinate[5][2].x + R, PointCoordinate[5][2].y + R);
		InsidePoint[2].setPosition(PointCoordinate[5][3].x + R, PointCoordinate[5][3].y + R);
		InsidePoint[3].setPosition(PointCoordinate[5][4].x + R, PointCoordinate[5][4].y + R);
		break;

	case 3:
		OutsidePoint[0].setPosition(PointCoordinate[1][10].x + R, PointCoordinate[1][10].y + R);
		OutsidePoint[1].setPosition(PointCoordinate[1][9].x + R, PointCoordinate[1][9].y + R);
		OutsidePoint[2].setPosition(PointCoordinate[0][9].x + R, PointCoordinate[0][9].y + R);
		OutsidePoint[3].setPosition(PointCoordinate[0][10].x + R, PointCoordinate[0][10].y + R);
		InsidePoint[0].setPosition(PointCoordinate[1][5].x + R, PointCoordinate[1][5].y + R);
		InsidePoint[1].setPosition(PointCoordinate[2][5].x + R, PointCoordinate[2][5].y + R);
		InsidePoint[2].setPosition(PointCoordinate[3][5].x + R, PointCoordinate[3][5].y + R);
		InsidePoint[3].setPosition(PointCoordinate[4][5].x + R, PointCoordinate[4][5].y + R);
		break;
	}

	/* HomePoint */
	HomePoint.setFillColor(sf::Color::Transparent);
	HomePoint.setRadius(R);
	HomePoint.setOrigin(R, R);
	HomePoint.condition = 0;
	HomePoint.setOutlineColor(color);
	HomePoint.setOutlineThickness(4);
	switch (index)
	{
	case 0:
		HomePoint.setPosition(PointCoordinate[6][10].x + R, PointCoordinate[6][10].y + R);
		HomePoint.index = 0;
		break;
	case 1:
		HomePoint.setPosition(PointCoordinate[10][4].x + R, PointCoordinate[10][4].y + R);
		HomePoint.index = 10;
		break;
	case 2:
		HomePoint.setPosition(PointCoordinate[4][0].x + R, PointCoordinate[4][0].y + R);
		HomePoint.index = 20;
		break;
	case 3:
		HomePoint.setPosition(PointCoordinate[0][6].x + R, PointCoordinate[0][6].y + R);
		HomePoint.index = 30;
		break;
	}
}

void Player::CreateTokens(sf::Vector2f PointCoordinate[11][11])
{
	float R{ 12. };
	for (size_t i{}; i < 4; i++)
	{
		token[i].position = i;
		token[i].PlayerIndex = index;
		token[i].index = i;
		token[i].setRadius(R);
		token[i].setFillColor(color);
		token[i].setOrigin(R, R);
		token[i].setPosition(OutsidePoint[i].getPosition());
		token[i].condition = -1; // out
	}
}