#include "Game.h"

Game::Game(int PlayerCondition0[], sf::Color PlayerColor0[])
{
	for (size_t i{}; i < 4; i++)
	{
		PlayerColor[i] = PlayerColor0[i];
		PlayerCondition[i] = PlayerCondition0[i];
	}
}

void Game::Run()
{
	/* Window */
	window.setFramerateLimit(30);
	
	/* Create */
	CreatePointsCoordinate();
	CreatePlayers();
	CreateOriginalPoints();
	CreateDiceButton();

	/* GameLoop */
	std::cout << "Game Started" << std::endl <<
		"Guide: Please click the dice then click one of your tokens that you wanna move." << std::endl;
	srand(time(0));
	SetHandout();
	while (window.isOpen())
	{
		ProcessEvents();

		if (theGameisRunning())
		{
			/* DiceNumber & MuteCheck */
			if (MouseClickDice) // player[handout].condition == -1: Auto dice for pc
				MouseClickDiceFunction();
				
			/* Move */
			if (TokenHasBeenChoosen) // is not mute
				TokenHasBeenChoosenFunction();

			WinCheck();

			/* Draw & Clear */
			window.clear(sf::Color::Black);
			DrawWindow();
			window.display();
		}
	}

	return;
}

Game::~Game()
{
}

void Game::ProcessEvents()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			window.close();
			break;

		case sf::Event::MouseButtonPressed:
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				if (WanttoClickDice)
				{
					if (sqrt(pow(event.mouseButton.x - DiceButton.getPosition().x, 2) + pow(event.mouseButton.y - DiceButton.getPosition().y, 2)) <= DiceButton.getRadius())
					{
						MouseClickDice = true;
						WanttoClickDice = false;
						break;
					}
				}

				else if (WanttoChooseToken)
				{
					for (size_t j = 0; j < 4; j++)
					{
						if (sqrt(pow(event.mouseButton.x - player[handout].token[j].getPosition().x, 2) + pow(event.mouseButton.y - player[handout].token[j].getPosition().y, 2)) <= player[handout].token[j].getRadius())
						{
							TokenNumber = j;
							TokenHasBeenChoosen = true;
							WanttoChooseToken = false;
							break;
						}
					}
				}
			}
		}
	}
	return;
}

void Game::CreatePlayers()
{
	for (size_t i{}; i < 4; i++)
	{
		if (PlayerCondition[i] != 0)
		{
			Player TempPlayer{ i, PlayerColor[i] };
			TempPlayer.condition = PlayerCondition[i];
			TempPlayer.CreatePoints(PointCoordinate);
			TempPlayer.CreateTokens(PointCoordinate);
			player[i] = TempPlayer;
		}

		else
		{
			Player TempPlayer{ i, sf::Color::Transparent };
			TempPlayer.condition = 0;
			player[i] = TempPlayer;
		}

	}

	return;
}

void Game::CreatePointsCoordinate()
{
	float dis{ 600 / 11 };
	float border{ 10 };

	for (size_t i{}; i < 11; i++)
		for (size_t j{}; j < 11; j++)
		{
			PointCoordinate[i][j].x = j*dis + border;
			PointCoordinate[i][j].y = i*dis + border;
		}
	return;
}

void Game::CreateDiceButton()
{
	DiceButton.setRadius(25.);
	DiceButton.setOrigin(DiceButton.getRadius(), DiceButton.getRadius());
	DiceButton.setPosition(700., 530.);
	DiceButton.setFillColor(sf::Color::Cyan);

	return;
}

void Game::CreateOriginalPoints()
{
	std::ifstream OriginalPointIndexFile{ "OriginalPointIndex.txt" };
	if (!OriginalPointIndexFile.is_open())
	{
		std::cout << "Error: Can not open OriginalPointIndex.txt." << std::endl;
		return;
	}

	float R{ 20. };
	for (size_t iter{}; iter < 40; iter++)
	{
		int i, j;
		OriginalPointIndexFile >> i >> j;

		OriginalPoint[iter].setFillColor(sf::Color::White);
		OriginalPoint[iter].setRadius(R);
		OriginalPoint[iter].setOrigin(R, R);
		OriginalPoint[iter].setPosition(PointCoordinate[i][j].x + R, PointCoordinate[i][j].y + R);
		OriginalPoint[iter].index = iter;
		OriginalPoint[iter].condition = 0;
	}
	OriginalPointIndexFile.close();

	return;
}

bool Game::theGameisRunning()
{
	/* The End Check */
	size_t OnPlayers{};
	for (size_t i = 0; i < 4; i++)
		if (player[i].condition != 0)
			OnPlayers += 1;
	if (OnPlayers == 1)
	{
		std::cout << "The End" << std::endl;
		window.close();
		return false;
	}

	return true;
}

void Game::SetHandout()
{
	if (DiceNumber != 6)
		do
			handout = (handout + 1) % 4;
	while (player[handout].condition == 0);

	std::cout << std::endl << "Handout: " << handout + 1 << std::endl;
	return;
}

bool Game::isMovable(unsigned int& TokenNum)
{	
	/* Do not attack your self in 0 condition */
	for (size_t i{}; i < 4; i++)
	{
		if (i == TokenNumber)
			continue;
		if (player[handout].token[i].condition == 0)
			if (player[handout].token[TokenNum].position + DiceNumber == player[handout].token[i].position)
				return false;
		if (player[handout].token[TokenNum].condition == -1 && DiceNumber == 6)
			if (player[handout].token[i].condition == 0 && player[handout].token[i].position == player[handout].HomePoint.index)
				return false;

	}

	/* Can not start the game without 6 */
	if (player[handout].token[TokenNum].condition == -1 && DiceNumber != 6)
		return false;

	/* Inside move */
	if (player[handout].token[TokenNum].condition == +1)
	{
		if (player[handout].token[TokenNum].position + DiceNumber >= 4)
			return false;

		/* Do not attack your self in both +1 condition */
		for (size_t i = 0; i < 4; i++)
		{
			if (i == TokenNumber)
				continue;
			if (player[handout].token[i].condition == +1)
				if (player[handout].token[TokenNum].position + DiceNumber == player[handout].token[i].position)
					return false;
		}
	}

	/* Ability to Go in */
	if (player[handout].token[TokenNum].condition == 0)
	{
		unsigned int DestinationPosition{ player[handout].token[TokenNum].position + DiceNumber }; // short style
		int index{};
		switch (player[handout].index)
		{
		case 0:
			index = DestinationPosition - 40;
			break;
		default:
			if (DestinationPosition < player[handout].HomePoint.index)
				index = DestinationPosition - player[handout].HomePoint.index;
			break;
		}
		if (index >= 4)
			return false;
		/* Do not attack your self in 0 & +1 condition */
		for (size_t i = 0; i < 4; i++)
		{
			if (i == TokenNumber)
				continue;
			if (player[handout].token[i].condition == +1)
				if (index == player[handout].token[i].position)
					return false;
		}
	}

	return true;
}

bool Game::isMute()
{
	for (size_t i{}; i < 4; i++)
	{
		if (isMovable(i))
			return false;
	}

	return true;
}

void Game::MoveToken()
{
	unsigned int& TempPosition = player[handout].token[TokenNumber].position; // short style
	
	/* Inside move */
	if (player[handout].token[TokenNumber].condition == +1)
	{
		TempPosition += DiceNumber;
		player[handout].token[TokenNumber].setPosition(player[handout].InsidePoint[TempPosition].getPosition());
	}

	/* Start with 6 */
	else if (player[handout].token[TokenNumber].condition == -1)
	{
		if (DiceNumber == 6)
		{
			player[handout].token[TokenNumber].condition = 0;
			TempPosition = player[handout].HomePoint.index;
			player[handout].token[TokenNumber].setPosition(player[handout].HomePoint.getPosition());
		}
	}

	/* Go in or not */
	else
	{		
		bool GoInFlag{ false };
		switch (handout)
		{
		case 0:
			if (TempPosition + DiceNumber >= 40)
				GoInFlag = true;
			break;
		default:
			if (TempPosition < player[handout].HomePoint.index && TempPosition + DiceNumber >= player[handout].HomePoint.index)
				GoInFlag = true;
			break;
		}
		if (GoInFlag)
		{
			int index{};
			switch (handout)
			{
			case 0:
				index = TempPosition + DiceNumber - 40;
				break;
			default:
				index = TempPosition + DiceNumber - player[handout].HomePoint.index;
				break;
			}
			player[handout].token[TokenNumber].condition = 1;
			TempPosition = index;
			player[handout].token[TokenNumber].setPosition(player[handout].InsidePoint[index].getPosition());
		}
		else
		{
			TempPosition = (TempPosition + DiceNumber) % 40;
			player[handout].token[TokenNumber].setPosition(OriginalPoint[TempPosition].getPosition());
		}
	}

	/* Attack */
	for (size_t i = 0; i < 4; i++) // players
	{
		if (i == handout)
			continue;

		if (player[i].condition != 0) // is on
			for (size_t j = 0; j < 4; j++) // tokens
			{
				if (player[i].token[j].condition == 0) // token is in the game
					if (player[i].token[j].position == TempPosition)
					{
						/* GoHome */
						int& TempIndex = player[i].token[j].index; // short style
						player[i].token[j].position = TempIndex;
						player[i].token[j].setPosition(player[i].OutsidePoint[TempIndex].getPosition());
						player[i].token[j].condition = -1;
						std::cout << "Player " << handout + 1 << " attacked Player " << i << "." << std::endl;
					}
			}
	}

	return;
}

void Game::WinCheck()
{
	bool WinFlag{ true };
	for (size_t j = 0; j < 4; j++)
	{
		if (player[handout].token[j].condition != 1) // is not inside
		{
			WinFlag = false;
			break;
		}
	}
	if (WinFlag)
	{
		player[handout].condition = 0; // set player off
		std::cout << "Player " << handout + 1 << " Wins" << std::endl;
	}

	return;
}

void Game::ChooseToken()
{
	/* First: select an isMovable token */
	for (size_t i = 0; i < 4; i++)
		if (isMovable(i))
			TokenNumber = i;

	/* Change the tokennumber considering the situation */
	for (size_t iter = 0; iter < 4; iter++) // this player tokens
		for (size_t i = 0; i < 4; i++) // other players
		{
			if (i == handout)
				continue;

			if (player[i].condition != 0) // is on
				for (size_t j = 0; j < 4; j++) // other players tokens
					/* if PC has a token than it can attack with that token, so choose that */
					if (player[i].token[j].condition == 0) // token is in the game
					{
						if (player[handout].token[iter].condition == 0)
						{
							if (player[i].token[j].position == (player[handout].token[iter].position + DiceNumber) % 40)
							{
								TokenNumber = iter;
								break;
							}
						}
						else if (player[handout].token[iter].condition == -1 && DiceNumber == 6)
						{
							if (player[i].token[j].position == player[handout].HomePoint.index)
							{
								TokenNumber = iter;
								break;
							}
						}
					}
		}

	TokenHasBeenChoosen = true;

	return;
}

void Game::DrawWindow()
{
	/* Points */
	for (size_t i = 0; i < 40; i++)
		window.draw(OriginalPoint[i]);
	for (size_t i = 0; i < 4; i++)
		if (player[i].condition != 0) // player is playing
			for (size_t j = 0; j < 4; j++)
			{
				window.draw(player[i].InsidePoint[j]);
				window.draw(player[i].OutsidePoint[j]);
				window.draw(player[i].HomePoint);
			}

	/* Tokens */
	for (size_t i = 0; i < 4; i++)
		if (player[i].condition != 0) // player is playing
			for (size_t j = 0; j < 4; j++)
				window.draw(player[i].token[j]);

	window.draw(DiceButton);

	return;
}

void Game::MouseClickDiceFunction()
{
	DiceNumber = static_cast<unsigned int> ((rand() % 6 + 1));
	std::cout << "DiceNumber: " << DiceNumber << std::endl;
	MouseClickDice = false;
	if (isMute())
	{
		std::cout << "You Are Mute." << std::endl;
		SetHandout();
		WanttoChooseToken = false;
		WanttoClickDice = true;
	}
	else
	{
		if (player[handout].condition == 1)
			WanttoChooseToken = true;
		else if (player[handout].condition == -1)
			ChooseToken();
	}
}

void Game::TokenHasBeenChoosenFunction()
{
	if (isMovable(TokenNumber))
	{
		MoveToken();
		SetHandout();
		WanttoClickDice = true;
	}
	else
	{
		std::cout << "This token is not movable." << std::endl;
		WanttoChooseToken = true;
	}

	TokenHasBeenChoosen = false; // is movable and !is movable both
}
