#pragma once
#include <SFML/Graphics.hpp>

class Token : public sf::CircleShape
{
public:
	Token();
	~Token();

	int index;
	int PlayerIndex;
	int condition; // -1: Out; 0: IntheGame; +1: In;
	unsigned int position;
};

