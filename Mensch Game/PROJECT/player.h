#pragma once
#include <SFML/Graphics.hpp>
#include "token.h"
#include "point.h"

class Player
{
public:
	Player(); // default constructor
	Player(size_t i, const sf::Color& PColor);
	void CreatePoints(sf::Vector2f PointCoordin[11][11]);
	void CreateTokens(sf::Vector2f PointCoordin[11][11]);
	~Player();

	size_t index;
	int condition; // -1: PC; 0: None; +1: Player
	sf::Color color;
	Point InsidePoint[4];
	Point OutsidePoint[4];
	Token token[4];
	Point HomePoint;
};
